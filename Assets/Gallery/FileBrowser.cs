﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;
public class FileBrowser  {


	public Action<FileBrowser> OnCurrentDirectoryChange;

	DirectoryInfo _currentDirectory;
	public DirectoryInfo currentDirectory {
		get {
			return _currentDirectory;
		}
		set {
			if (_currentDirectory == value)
				return;

			_currentDirectory = value;
			
			FilesInCurrentDirectory = GetFilesInDirectory(currentDirectory, FileSearchPattern);
			LogicalDrives = GetDrives();
			DirectoriesInCurrentDirectory = GetDirectoriesInDirectory(currentDirectory);
			if (OnCurrentDirectoryChange != null)
				OnCurrentDirectoryChange?.Invoke(this);
		}
	}

	public FileInfo[] FilesInCurrentDirectory;
	public DirectoryInfo[] DirectoriesInCurrentDirectory;
	public DirectoryInfo[] LogicalDrives;
	public string FileSearchPattern = "*";

	public FileInfo[] GetFilesInDirectory(DirectoryInfo directory, string searchPattern) {
		return directory.GetFiles(searchPattern);
	}

	public DirectoryInfo[] GetDirectoriesInDirectory(DirectoryInfo directory) {
		DirectoryInfo[] dir = directory.GetDirectories();
		var filltred = dir.Where(d => !d.Attributes.HasFlag(FileAttributes.Hidden));
		return filltred.ToArray();
	}

	public DirectoryInfo[] GetDrives() {

		string[] drivesNames = Directory.GetLogicalDrives();
		DirectoryInfo[] drives = new DirectoryInfo[drivesNames.Length];

		for (int i = 0; i < drivesNames.Length; i++) 
			drives[i] = new DirectoryInfo(drivesNames[i]);
		
		return drives;
	}

	public void SetCurrentDirectory(string path) {
		try {
			currentDirectory = new DirectoryInfo(path);

		} catch (Exception e) {
			Debug.LogError(e);
		}
	}
}
