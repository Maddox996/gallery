﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Linq;


public class ImageLoader {

	const string THUMB_STRING = "THUMB\\";
	const string FILE_SEARCH_EXTANSIONS = "*.jpg | *.png";

	public Action OnLoadedEvent;
	public Action OnNoImagesFound;
	public Action OnLoadingAborted;

	uint LONGER_EDGE;

	public bool saveThumbs;

	public ImageLoader(uint LongerEdgeValue, bool _saveThumbs) {
		LONGER_EDGE = LongerEdgeValue;
		saveThumbs = _saveThumbs;
		imageLoaderState = ImageLoaderStates.WAITING;
		OnLoadingAborted += (() => Debug.Log("xD"));
	}
    
    public Dictionary<string, Texture2D> LoadedThumbnailsTextures = new Dictionary<string, Texture2D>();

    List<string> RootImagesPathesToLoad = new List<string>();
    List<string> ThumbsImagesPathesToLoad = new List<string>();

    List<string> FilesNameToSave = new List<string>();
    List<string> FilesNameToDelete = new List<string>();

	public enum ImageLoaderStates { WAITING, LOADING, ABORTING, LOADED, ABORTED }
	public ImageLoaderStates imageLoaderState;


	public void Clear() {
		RootImagesPathesToLoad.Clear();
		ThumbsImagesPathesToLoad.Clear();
		FilesNameToDelete.Clear();
		FilesNameToSave.Clear();
		LoadedThumbnailsTextures.Clear();
	}

	public bool IsAbortingOrLoading() {
		return (imageLoaderState == ImageLoaderStates.LOADING || imageLoaderState == ImageLoaderStates.ABORTING);
	}

	public async void LoadImages(string folderPath, Action<Texture2D> ActionOnTexture = null) {

		if (!Directory.Exists(folderPath)){
			Debug.LogError(folderPath + " path not found.");
			return;
		}

		if (folderPath[folderPath.Length - 1] != '/' && folderPath[folderPath.Length - 1] != '\\') 
			folderPath += "/";
	
		if (IsAbortingOrLoading())
			return;

		RootImagesPathesToLoad = GetImagesPathesToLoad(folderPath);

		if (RootImagesPathesToLoad.Count < 1) {
			OnLoadedEvent?.Invoke();
			OnNoImagesFound?.Invoke();
			return;
		}

		string thumbPath = folderPath + THUMB_STRING;
		List<string> FilesNamesInThumbFolder = new List<string>();
		List<string> FilesNamesInRootFolder = new List<string>();


		if (saveThumbs && !(Path.GetDirectoryName(folderPath).Contains(THUMB_STRING))) {

			if (!Directory.Exists(thumbPath))
				Directory.CreateDirectory(thumbPath);

			ThumbsImagesPathesToLoad = GetImagesPathesToLoad(thumbPath);

			foreach (string s in ThumbsImagesPathesToLoad)
				FilesNamesInThumbFolder.Add(Path.GetFileName(s));

			foreach (string s in RootImagesPathesToLoad)
				FilesNamesInRootFolder.Add(Path.GetFileName(s));

			FilesNameToSave = Enumerable.Except<string>(FilesNamesInRootFolder, FilesNamesInThumbFolder).ToList();
			FilesNameToDelete = Enumerable.Except<string>(FilesNamesInThumbFolder, FilesNamesInRootFolder).ToList();
			await DeleteThumb(FilesNameToDelete, folderPath);
			ThumbsImagesPathesToLoad = GetImagesPathesToLoad(thumbPath); //array update after delete

			imageLoaderState = ImageLoaderStates.LOADING;

			foreach (string s in ThumbsImagesPathesToLoad) {

				if (ShouldBreak())
					break;
					
				await LoadFromThumbs(s);

				if (ShouldBreak())
					break;

				ActionOnTexture?.Invoke(LoadedThumbnailsTextures[s]);
			}

			foreach (string s in FilesNameToSave) {

				if (ShouldBreak())
					break;

				await LoadFromRoot(folderPath + s);

				if (ShouldBreak())
					break;

				ActionOnTexture?.Invoke(LoadedThumbnailsTextures[folderPath + s]);

			}

		} else {

			imageLoaderState = ImageLoaderStates.LOADING;

			foreach (string s in RootImagesPathesToLoad) {

				if (ShouldBreak())
					break;

				await LoadFromRoot(s);

				if (ShouldBreak())
					break;

				ActionOnTexture?.Invoke(LoadedThumbnailsTextures[s]);
			}
		}

		if(imageLoaderState == ImageLoaderStates.LOADING) {
			imageLoaderState = ImageLoaderStates.LOADED;

			OnLoadedEvent?.Invoke();
		}
	}

	List<string> GetImagesPathesToLoad(string folderPath) {

		List<string> imagesPathes = new List<string>();
		DirectoryInfo dir = new DirectoryInfo(folderPath);
		
		imagesPathes = GetFiles(folderPath, FILE_SEARCH_EXTANSIONS, SearchOption.TopDirectoryOnly);

		return imagesPathes;
	}

	async Task LoadFromRoot(string path) {

        try {
            Image image = await GetImage(path);
            image = await ResizeImage(image);
            if(saveThumbs)
				await SaveThumb(path, image);
            byte[] imageInf = await ImageToByteArray(image);
            LoadedThumbnailsTextures.Add(path, SetupTexture(image, imageInf));
        }
        catch (Exception ex) {
            Debug.Log(ex.Message);
        }
    }

    async Task LoadFromThumbs(string path)
    {
        try {
            Image image = await GetImage(path);
            byte[] imageInf = await ImageToByteArray(image);
            LoadedThumbnailsTextures.Add(path, SetupTexture(image, imageInf));
        }
        catch (Exception ex) {
            Debug.Log(ex.Message);
        }
    }

    async Task SaveThumb(string path, Image img)
    {
        await Task.Run(() =>
        {
            try
            {
                path = Path.GetDirectoryName(path) + "\\" + THUMB_STRING + Path.GetFileName(path);
                Debug.Log("Zapisuje.. " + path);
                img.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            }
            catch(Exception e)
            {
                Debug.Log(e);
            }
            
        });
    }

    async Task DeleteThumb(List<string> FilesNameToDelete, string rootPath)
    {
        await Task.Run(() =>
        {
            foreach(string s in FilesNameToDelete)
            {
                try {
                    File.Delete(rootPath + THUMB_STRING + s);
                }
                catch (Exception e) { 
                    Debug.Log(e);
                }
            }
        });
    }

    async Task<Image> GetImage(string path) {

        return await Task.Run<Image>(() =>
        {
            Bitmap image = null;
    
            try {
                image = (Bitmap)Image.FromFile(path, true);
            }
            catch (System.IO.FileNotFoundException) {
                throw new Exception("There was an error while loading an image: " + path);
            }
            return image;
        });
    }

    async Task<Image> ResizeImage(Image image) {
        return await Task.Run<Image>(() =>
        {
            Vector2 ResizedSize = CalculateSize(image);
            Image resizedImage = image.GetThumbnailImage((int)ResizedSize.x, (int)(ResizedSize.y), null, IntPtr.Zero);
            return resizedImage;
        });
    }

    Texture2D SetupTexture(Image resizedImage, byte[] imageInf) {

        if (resizedImage.Height > LONGER_EDGE || resizedImage.Width > LONGER_EDGE)
            throw new Exception("You cannot setup texture on not resized image!");

        Texture2D texture = new Texture2D(resizedImage.Width, resizedImage.Height);
        texture.LoadImage(imageInf);
        return texture;
    }

    Vector2 CalculateSize(Image image) {

        bool HeightIsGreater = false;
        
        if (image.Height < LONGER_EDGE && image.Width < LONGER_EDGE)
            return new Vector2(image.Width, image.Height);

        if (image.Height == image.Width)
            return new Vector2(LONGER_EDGE, LONGER_EDGE);

        float LongerValue = image.Height > image.Width ? image.Height : image.Width;
        float multiplifier = LONGER_EDGE / LongerValue;

        HeightIsGreater = LongerValue == image.Height;

        if (HeightIsGreater)
            return new Vector2(image.Width * multiplifier, LONGER_EDGE);

        return new Vector2(LONGER_EDGE, image.Height * multiplifier);
    }

    async Task<byte[]> ImageToByteArray(Image imageIn){
        return await Task.Run<byte[]>(() =>
        {
            var ms = new MemoryStream();
            imageIn.Save(ms, imageIn.RawFormat);
            return ms.ToArray();
        });
    } 

	public void AbortLoading() {
		if(imageLoaderState == ImageLoaderStates.LOADING)
			imageLoaderState = ImageLoaderStates.ABORTING;
	}

	public List<string> GetFiles(string path, string searchPattern, SearchOption searchOption) {
		string[] searchPatterns = searchPattern.Split('|');
		List<string> files = new List<string>();
		foreach (string sp in searchPatterns)
			files.AddRange(System.IO.Directory.GetFiles(path, sp, searchOption));
		return files;
	}

	public bool ShouldBreak() {
		if (imageLoaderState == ImageLoaderStates.ABORTING) {
			imageLoaderState = ImageLoaderStates.ABORTED;
			OnLoadingAborted?.Invoke();
			return true;
		}

		return false;
	}
}
