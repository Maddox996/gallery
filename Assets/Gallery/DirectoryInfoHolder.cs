﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.EventSystems;

public class DirectoryInfoHolder : MonoBehaviour, IPointerClickHandler {

	public FileBrowser fileBrowser;
	public DirectoryInfo directoryInfo;

	public void OnPointerClick(PointerEventData eventData) {
		fileBrowser.currentDirectory = directoryInfo;
	}
}
