﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GalleryItem : MonoBehaviour, IPointerClickHandler {

	[HideInInspector]
	public Gallery gallery;

	public void OnPointerClick(PointerEventData eventData) {
		gallery.currentObject = this;
	}
}
