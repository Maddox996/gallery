﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Gallery))]
public class GalleryPreview : MonoBehaviour {

	Gallery gallery;
	RawImage img;

	[SerializeField, HideInInspector]
	GameObject preview;
	[SerializeField, HideInInspector]
	AspectRatioFitter fitter;

	private void Reset() {
		preview = CreatePreview();		
	}

	private void OnEnable() {
		if (preview == null) {
			Debug.LogError("Preview GameObject not found, you should reset the GalleryPreview script.");
			return;
		}

		gallery = GetComponent<Gallery>();
		img = preview.transform.GetComponentInChildren<RawImage>();
		gallery.OnItemSelected += UpdatePreviewImage;
		gallery.OnGalleryRestart += Restart;
	}

	public void Restart() {
		img.texture = Texture2D.whiteTexture;
	}

	void UpdatePreviewImage(GalleryItem g) {
		var t = g.GetComponentInChildren<RawImage>().texture;
		img.texture = t;
		fitter.aspectRatio = ((float)t.width) / t.height;

	}

	GameObject CreatePreview() {

		GameObject galleryItemParent = new GameObject("GalleryPreviewPrefab");
		GameObject galleryItem = new GameObject("GalleryPreviewContent");

		galleryItemParent.AddComponent<RectTransform>();
		galleryItem.AddComponent<RawImage>();
		fitter = galleryItem.AddComponent<AspectRatioFitter>();
		fitter.aspectMode = AspectRatioFitter.AspectMode.FitInParent;
		galleryItem.transform.SetParent(galleryItemParent.transform);
		galleryItemParent.transform.SetParent(transform);
		return galleryItemParent;
	}
}
