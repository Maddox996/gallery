﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System;
public class Gallery : MonoBehaviour {

	public enum GalleryStates { WAITING, LOADING, LOADED};
	public enum GalleryLoadOptions { LOAD_FROM_PATH, LOAD_FROM_PREDEFINED_LIST};
	public Action<GalleryItem> OnItemSelected;
	public Action<Gallery> OnGalleryLoaded;
	public Action OnGalleryRestart;

	[SerializeField, Tooltip("Image longer edge to scale to (working only with LOAD_FROM_PATH) - assign this before generating gallery.")]
	uint LongerEdge = 1024;
	[SerializeField]
	float scrollSpeed = 25.0f;
	public GalleryLoadOptions galleryLoadOptions;
	[SerializeField]
	public string path = "C://Images//";
	public List<Texture2D> Texture2DPredefinedList;
	[HideInInspector]
	public GalleryStates galleryState = GalleryStates.WAITING;
	GridLayoutGroup gridLayoutGroup;
	public ImageLoader imageLoader;
	public bool ShouldSaveThumbs = true;
	public bool GenerateOnEnable = false;
	Image backgroundImage, ScrollViewImage, SlidingAreaImage, HandleImage;
	GalleryItem _currentObject;
	ScrollRect scrollRect;

	[SerializeField, HideInInspector]
	GameObject GalleryItem;

	private void Reset() {
		GalleryItem = CreateGalleryItem();
		GalleryItem.SetActive(false);
		GalleryItem.transform.SetParent(transform);
		CreateGalleryStructure().transform.SetParent(transform);
		backgroundImage.color = new Color(255.0f, 255.0f, 255.0f, 0.3f);
	}

	private void OnValidate() {

		if (scrollRect == null)
			scrollRect = GetComponentInChildren<ScrollRect>();

		scrollRect.scrollSensitivity = scrollSpeed;
		if (imageLoader != null)
			ShouldGallerySaveThumbs(ShouldSaveThumbs);
	}

	GameObject CreateGalleryItem() {

		GameObject galleryItemParent = new GameObject("GalleryItemPrefab");
		GameObject galleryItem = new GameObject("GalleryItemContent");

		galleryItemParent.AddComponent<RectTransform>();
		galleryItem.AddComponent<RawImage>();
		galleryItem.AddComponent<AspectRatioFitter>().aspectMode = AspectRatioFitter.AspectMode.FitInParent;
		galleryItem.AddComponent<GalleryItem>();
		galleryItem.transform.SetParent(galleryItemParent.transform);

		return galleryItemParent;
	}

	GameObject CreateGalleryStructure() {

		GameObject background = new GameObject();
		background.name = "Gallery Structure (background image)";
		backgroundImage = background.AddComponent<Image>();
		background.GetComponent<RectTransform>().sizeDelta = new Vector2(1000.0f, 600.0f);
		
		GameObject ScrollView = new GameObject();
		ScrollView.name = "ScrollView";
		ScrollViewImage = ScrollView.AddComponent<Image>();
		ScrollViewImage.color = new Color(255.0f, 255.0f, 255.0f, 0.3f);
		ScrollViewImage.GetComponent<RectTransform>().sizeDelta = new Vector2(400.0f, 300.0f);
		var scrollRect = ScrollView.AddComponent<ScrollRect>();
		scrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
		scrollRect.horizontal = false;

		GameObject Viewport = new GameObject();
		Viewport.name = "Viewport";
		Viewport.AddComponent<Image>();
		Viewport.AddComponent<Mask>().showMaskGraphic = false;
		var rectView = Viewport.GetComponent<RectTransform>();
		rectView.anchorMin = new Vector2(0, 0);
		rectView.anchorMax = new Vector2(1, 1);
		rectView.pivot = new Vector2(0f, 1f);

		GameObject Content = new GameObject();
		Content.name = "Content";
		var GLG = Content.AddComponent<GridLayoutGroup>();
		Content.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 1.0f);
		GLG.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
		GLG.constraintCount = 3;
		var contentSizeFitter = Content.AddComponent<ContentSizeFitter>();

		GameObject ScrollBarHorizontal = new GameObject();
		ScrollBarHorizontal.name = "Scrollbar Vertical";
		SlidingAreaImage = ScrollBarHorizontal.AddComponent<Image>();
		var scrollBar = ScrollBarHorizontal.AddComponent<Scrollbar>();
		var ScrollBarRectT = scrollBar.GetComponent<RectTransform>();
		ScrollBarRectT.anchorMin = new Vector2(1.0f, 0);
		ScrollBarRectT.anchorMax = new Vector2(1.0f, 1.0f);
		ScrollBarRectT.pivot = new Vector2(1.0f, 1.0f);
		ScrollBarRectT.offsetMax = new Vector2(200.0f, 150.0f);
		ScrollBarRectT.offsetMin = new Vector2(180.0f, -133.0f);
		scrollBar.SetDirection(Scrollbar.Direction.BottomToTop, false);

		GameObject SlidingArea = new GameObject();
		SlidingArea.name = "Sliding area";
		var SlidingAreaRectT = SlidingArea.AddComponent<RectTransform>();
		SlidingAreaRectT.anchorMin = new Vector2(0, 0);
		SlidingAreaRectT.anchorMax = new Vector2(1, 1);
		SlidingAreaRectT.pivot = new Vector2(0.5f, 0.5f);
		SlidingAreaRectT.offsetMax = new Vector2(190.0f, 140.0f);
		SlidingAreaRectT.offsetMin = new Vector2(190.0f, -123.0f);

		GameObject Handle = new GameObject();
		Handle.name = "Handle";
		HandleImage = Handle.AddComponent<Image>();
		var handleRect = Handle.GetComponent<RectTransform>();
		handleRect.anchorMin = Vector2.zero;
		handleRect.anchorMax = Vector2.one;
		handleRect.pivot = new Vector2(0.5f, 0.5f);
		handleRect.offsetMax = new Vector2(200.0f, 150.0f);
		handleRect.offsetMin = new Vector2(180.0f, -133.0f);

		Content.transform.SetParent(Viewport.transform);
		Viewport.transform.SetParent(ScrollView.transform);
		ScrollView.transform.SetParent(background.transform);
		Handle.transform.SetParent(SlidingArea.transform);
		SlidingArea.transform.SetParent(ScrollBarHorizontal.transform);
		ScrollBarHorizontal.transform.SetParent(ScrollView.transform);

		
		scrollRect.content = Content.transform.GetComponent<RectTransform>();
		scrollRect.viewport = Viewport.transform.GetComponent<RectTransform>();
		scrollRect.verticalScrollbar = scrollBar;
		contentSizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
		contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
		scrollBar.targetGraphic = Handle.GetComponent<Image>();
		scrollBar.handleRect = Handle.GetComponent<RectTransform>();

		return background;
	}

	public GalleryItem currentObject
    {
        get { return _currentObject; }
        set {
			_currentObject = value;

			if (_currentObject != null)
            {
				OnItemSelected?.Invoke(value);
            }
        }
    }

	void FireOnGalleryLoadedEvent() {
		OnGalleryLoaded?.Invoke(this);
		galleryState = GalleryStates.LOADED;
	}

    public void GenerateGallery()
    {

		if (imageLoader == null)
			InitImageLoader();

		if(galleryState == GalleryStates.LOADING) {
			Debug.LogError("Gallery is currently loading..");
			return;
		}

		if (galleryState == GalleryStates.LOADED) {
			Debug.LogError("You should RestartGallery(), before generating once again.");
			return;
		}

		if (gridLayoutGroup == null)
			gridLayoutGroup = GetComponentInChildren<GridLayoutGroup>();

		galleryState = GalleryStates.LOADING;
		
		if(galleryLoadOptions == GalleryLoadOptions.LOAD_FROM_PATH) {
			imageLoader.LoadImages(path, (t) => {
				if(galleryState == GalleryStates.LOADING)
					SpawnGalleryItem(t);
			});
		}else if(galleryLoadOptions == GalleryLoadOptions.LOAD_FROM_PREDEFINED_LIST) {
			foreach(Texture2D t in Texture2DPredefinedList) {
				if (!(galleryState == GalleryStates.LOADING))
					break;
				SpawnGalleryItem(t);
			}
			FireOnGalleryLoadedEvent();
		}
    }

	void SpawnGalleryItem(Texture2D t) {

		if (t == null)
			return;

		if (galleryState == GalleryStates.LOADING) {

			if (gridLayoutGroup == null)
				return;

			GameObject g = Instantiate(GalleryItem, gridLayoutGroup.transform);
			g.GetComponentInChildren<UnityEngine.UI.RawImage>().texture = t;
			g.GetComponentInChildren<UnityEngine.UI.AspectRatioFitter>().aspectRatio = ((float)t.width) / t.height;
			g.GetComponentInChildren<GalleryItem>().gallery = this;
			g.SetActive(true);

		}
	}
	
	public void AbortLoading() {
		if (galleryState == GalleryStates.LOADING) {
			galleryState = GalleryStates.LOADED;
			imageLoader.AbortLoading();
			RestartGallery();
		}

	}

	public void RestartGallery() {

		if (galleryState == GalleryStates.LOADING || galleryState == GalleryStates.WAITING) {
			Debug.LogError("You can only restart loaded gallery");
			return;
		}

		if(imageLoader != null)
			imageLoader.Clear();

		GalleryItem[] galleryItems = transform.GetComponentsInChildren<GalleryItem>();
		foreach (GalleryItem g in galleryItems) {
			Destroy(g.transform.parent.gameObject);
		}

		OnGalleryRestart?.Invoke();
		galleryState = GalleryStates.WAITING;
	}

	void InitImageLoader() {
		if (imageLoader == null) {
			imageLoader = new ImageLoader(LongerEdge, ShouldSaveThumbs);
			imageLoader.OnLoadedEvent += FireOnGalleryLoadedEvent;
			imageLoader.OnNoImagesFound += ( () => Debug.LogError("Images not found"));
		}
	}

	public void ShouldGallerySaveThumbs(bool should) {

		if(galleryState == GalleryStates.LOADING) {
			Debug.LogError("Cant change image loader settings during loading");
			return;
		}
		if (imageLoader == null)
			InitImageLoader();

		ShouldSaveThumbs = should;
		imageLoader.saveThumbs = should;
	}

	private void Start() {
		if(GenerateOnEnable)
			GenerateGallery();
	}
}
