﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using UnityEditor;

public class GalleryFileBrowser : MonoBehaviour {

	Gallery gallery;


	[SerializeField, Header("Be careful with ShouldSaveThumbs it can cause a mess.")]
	Sprite backImage;
	[SerializeField]
	Sprite driveImage, directoryImage;
	Image backgroundImage, ScrollViewImage, SlidingAreaImage, HandleImage;
	GridLayoutGroup gridLayoutGroup;
	[SerializeField, HideInInspector]
	Scrollbar scrollBar;
	[SerializeField, HideInInspector]
	GameObject FileBrowserGameObject,  ButtonGameObject, InputFieldGameObject;

	FileBrowser myFileBrowser;
	[SerializeField, HideInInspector]
	InputField inputField;
	[SerializeField]
	public string defaultPath = "C://";

	private void Reset() {
		FileBrowserGameObject = CreateFileBrowserScrollview();
		FileBrowserGameObject.transform.SetParent(transform);
		ButtonGameObject = CreateDirectoryButton();
		ButtonGameObject.transform.SetParent(transform);
		ButtonGameObject.SetActive(false);
		backgroundImage.color = new Color(255.0f, 255.0f, 255.0f, 0.3f);
		InputFieldGameObject = CreateCurrentDirectoryText();
		InputFieldGameObject.transform.SetParent(transform);
		inputField = InputFieldGameObject.GetComponentInChildren<InputField>();
		inputField.text = "current directory";
	}

	private void OnEnable() {
		gallery = GetComponent<Gallery>();
		myFileBrowser = new FileBrowser();
		gallery.ShouldGallerySaveThumbs(false);
		myFileBrowser.SetCurrentDirectory(defaultPath);
		gallery.path = defaultPath;
		myFileBrowser.OnCurrentDirectoryChange += SpawnDirectoriesButtons;
		myFileBrowser.OnCurrentDirectoryChange += UpdateGallery;
		myFileBrowser.OnCurrentDirectoryChange += UpdateTextInput;
		inputField.onEndEdit.AddListener(GoToDirectory);
		inputField.text = myFileBrowser.currentDirectory.FullName;
		SpawnDirectoriesButtons(myFileBrowser);
	}

	public void GoToDirectory(string s) {
		myFileBrowser.SetCurrentDirectory(s);
	}
	
	public GameObject CreateCurrentDirectoryText() {
		GameObject background = new GameObject("Current directory");
		background.AddComponent<Image>().color = new Color(255.0f, 255.0f, 255.0f, 0.0f);

		GameObject textInput = new GameObject("Text input");
		var text = textInput.AddComponent<Text>();
		text.supportRichText = false;
		text.color = Color.black;
		textInput.AddComponent<InputField>().textComponent = text;
		textInput.transform.SetParent(background.transform);
		
		return background;
	}

	public GameObject CreateFileBrowserScrollview() {
		GameObject background = new GameObject();
		background.name = "Filebrowser Structure";
		backgroundImage = background.AddComponent<Image>();
		background.GetComponent<RectTransform>().sizeDelta = new Vector2(1000.0f, 600.0f);

		GameObject ScrollView = new GameObject();
		ScrollView.name = "ScrollView";
		ScrollViewImage = ScrollView.AddComponent<Image>();
		ScrollViewImage.color = new Color(255.0f, 255.0f, 255.0f, 0.3f);
		ScrollViewImage.GetComponent<RectTransform>().sizeDelta = new Vector2(400.0f, 300.0f);
		var scrollRect = ScrollView.AddComponent<ScrollRect>();
		scrollRect.verticalScrollbarVisibility = ScrollRect.ScrollbarVisibility.AutoHideAndExpandViewport;
		scrollRect.horizontal = false;
		scrollRect.scrollSensitivity = 25.0f;

		GameObject Viewport = new GameObject();
		Viewport.name = "Viewport";
		Viewport.AddComponent<Image>();
		Viewport.AddComponent<Mask>().showMaskGraphic = false;
		var rectView = Viewport.GetComponent<RectTransform>();
		rectView.anchorMin = new Vector2(0, 0);
		rectView.anchorMax = new Vector2(1, 1);
		rectView.pivot = new Vector2(0f, 1f);

		GameObject Content = new GameObject();
		Content.name = "Content";
		var GLG = Content.AddComponent<GridLayoutGroup>();
		Content.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 1.0f);
		GLG.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
		GLG.constraintCount = 1;
		var contentSizeFitter = Content.AddComponent<ContentSizeFitter>();

		GameObject ScrollBarHorizontal = new GameObject();
		ScrollBarHorizontal.name = "Scrollbar Vertical";
		SlidingAreaImage = ScrollBarHorizontal.AddComponent<Image>();
		scrollBar = ScrollBarHorizontal.AddComponent<Scrollbar>();
		var ScrollBarRectT = scrollBar.GetComponent<RectTransform>();
		ScrollBarRectT.anchorMin = new Vector2(1.0f, 0);
		ScrollBarRectT.anchorMax = new Vector2(1.0f, 1.0f);
		ScrollBarRectT.pivot = new Vector2(1.0f, 1.0f);
		ScrollBarRectT.offsetMax = new Vector2(200.0f, 150.0f);
		ScrollBarRectT.offsetMin = new Vector2(180.0f, -133.0f);
		scrollBar.SetDirection(Scrollbar.Direction.BottomToTop, false);

		GameObject SlidingArea = new GameObject();
		SlidingArea.name = "Sliding area";
		var SlidingAreaRectT = SlidingArea.AddComponent<RectTransform>();
		SlidingAreaRectT.anchorMin = new Vector2(0, 0);
		SlidingAreaRectT.anchorMax = new Vector2(1, 1);
		SlidingAreaRectT.pivot = new Vector2(0.5f, 0.5f);
		SlidingAreaRectT.offsetMax = new Vector2(190.0f, 140.0f);
		SlidingAreaRectT.offsetMin = new Vector2(190.0f, -123.0f);

		GameObject Handle = new GameObject();
		Handle.name = "Handle";
		HandleImage = Handle.AddComponent<Image>();
		var handleRect = Handle.GetComponent<RectTransform>();
		handleRect.anchorMin = Vector2.zero;
		handleRect.anchorMax = Vector2.one;
		handleRect.pivot = new Vector2(0.5f, 0.5f);
		handleRect.offsetMax = new Vector2(200.0f, 150.0f);
		handleRect.offsetMin = new Vector2(180.0f, -133.0f);

		Content.transform.SetParent(Viewport.transform);
		Viewport.transform.SetParent(ScrollView.transform);
		ScrollView.transform.SetParent(background.transform);
		Handle.transform.SetParent(SlidingArea.transform);
		SlidingArea.transform.SetParent(ScrollBarHorizontal.transform);
		ScrollBarHorizontal.transform.SetParent(ScrollView.transform);


		scrollRect.content = Content.transform.GetComponent<RectTransform>();
		scrollRect.viewport = Viewport.transform.GetComponent<RectTransform>();
		scrollRect.verticalScrollbar = scrollBar;
		contentSizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
		contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
		scrollBar.targetGraphic = Handle.GetComponent<Image>();
		scrollBar.handleRect = Handle.GetComponent<RectTransform>();
		background.GetComponent<RectTransform>().sizeDelta = new Vector2(200.0f, 600.0f);
		ScrollViewImage.GetComponent<RectTransform>().sizeDelta = new Vector2(200.0f, 600.0f);

		return background;
	}

	public GameObject CreateDirectoryButton() {

		GameObject button = new GameObject("Directory button");
		var img = button.AddComponent<Image>();
		img.color = new Color(255.0f, 255.0f, 255.0f, 0.3f);
		GameObject buttonImage = new GameObject("Button image");
		var bi = buttonImage.AddComponent<Image>();
		bi.GetComponent<RectTransform>().sizeDelta = new Vector2(25.0f, 25.0f);
		GameObject buttonText = new GameObject();
		var textComponent = buttonText.AddComponent<Text>();
		textComponent.text = "DIRECTORY PATH";
		textComponent.color = Color.black;
		button.AddComponent<DirectoryInfoHolder>();

		buttonText.transform.SetParent(button.transform);
		buttonImage.transform.SetParent(button.transform);
		return button;
	}

	void SpawnDirectoriesButtons(FileBrowser fileBrowser) {

		CleanUpContent();

		if (gridLayoutGroup == null)
			gridLayoutGroup = FileBrowserGameObject.GetComponentInChildren<GridLayoutGroup>();

		if(fileBrowser.currentDirectory.Parent != null) {
			var backButton = Instantiate(ButtonGameObject, gridLayoutGroup.transform);
			backButton.GetComponentInChildren<Text>().text = fileBrowser.currentDirectory.Parent.Name;
			var bbHolder = backButton.GetComponentInChildren<DirectoryInfoHolder>();
			bbHolder.fileBrowser = myFileBrowser;
			bbHolder.directoryInfo = myFileBrowser.currentDirectory.Parent;
			backButton.GetComponentsInChildren<Image>()[1].sprite = backImage;
			backButton.SetActive(true);

		} else {
			foreach (DirectoryInfo d in fileBrowser.LogicalDrives) {
				var button = Instantiate(ButtonGameObject, gridLayoutGroup.transform);
				button.GetComponentInChildren<Text>().text = d.Name;
				var holder = button.GetComponentInChildren<DirectoryInfoHolder>();
				holder.fileBrowser = myFileBrowser;
				holder.directoryInfo = d;
				button.GetComponentsInChildren<Image>()[1].sprite = driveImage;
				button.SetActive(true);
			}
		}

		if (fileBrowser.DirectoriesInCurrentDirectory == null)
			return;

		foreach (DirectoryInfo d in fileBrowser.DirectoriesInCurrentDirectory) {
			var button = Instantiate(ButtonGameObject, gridLayoutGroup.transform);
			button.GetComponentInChildren<Text>().text = d.Name;
			var holder = button.GetComponentInChildren<DirectoryInfoHolder>();
			holder.fileBrowser = myFileBrowser;
			holder.directoryInfo = d;
			button.GetComponentsInChildren<Image>()[1].sprite = directoryImage;
			button.SetActive(true);
		}
	}

	void UpdateGallery(FileBrowser browser) { 

		if(gallery.galleryState == Gallery.GalleryStates.LOADED)
			gallery.RestartGallery();

		if (gallery.galleryState == Gallery.GalleryStates.LOADING) {
			gallery.AbortLoading();
		}

		gallery.path = browser.currentDirectory.FullName + "\\";
		InvokeRepeating("TryToGenerateGallery", 0.0f, 0.1f);		
	}

	void TryToGenerateGallery() {
		if (!gallery.imageLoader.IsAbortingOrLoading()) {
			gallery.GenerateGallery();
			CancelInvoke();
		}
	}

	void CleanUpContent() {

		if (gridLayoutGroup == null)
			gridLayoutGroup = FileBrowserGameObject.GetComponentInChildren<GridLayoutGroup>();

		for (int i = 0; i < gridLayoutGroup.transform.childCount; i++) {
			Destroy(gridLayoutGroup.transform.GetChild(i).gameObject);
		}

		if (scrollBar == null)
			scrollBar = FileBrowserGameObject.GetComponentInChildren<Scrollbar>();

		scrollBar.value = 1.0f;
	}

	void UpdateTextInput(FileBrowser browser) {
		inputField.text = browser.currentDirectory.FullName;
	}

	
}
