﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(Gallery))]
public class GalleryAcceptButton : MonoBehaviour {


	Gallery gallery;
	[SerializeField, HideInInspector]
	Button button;

	[SerializeField, HideInInspector]
	GameObject acceptButton;

	public Action<GalleryItem> OnAccept;

	private void OnEnable() {

		if (acceptButton == null || button == null) {
			Debug.LogError("Accept button GameObject not found, you should reset the GalleryAcceptButton script.");
			return;
		}
		 
		gallery = GetComponent<Gallery>();
		button.onClick.AddListener(FireEvent);
	}

	void Reset() {
		acceptButton = CreatePreview();
	}

	void FireEvent() {
		OnAccept?.Invoke(gallery.currentObject);
	}

	GameObject CreatePreview() {
		GameObject galleryItemParent = new GameObject("GalleryAcceptButonPrefab");
		GameObject galleryItem = new GameObject("GalleryAcceptButtonContent");

		galleryItemParent.AddComponent<RectTransform>();
		galleryItem.AddComponent<Image>();
		button = galleryItem.AddComponent<Button>();
		galleryItem.transform.SetParent(galleryItemParent.transform);
		galleryItemParent.transform.SetParent(transform);
		return galleryItemParent;
	}
}
